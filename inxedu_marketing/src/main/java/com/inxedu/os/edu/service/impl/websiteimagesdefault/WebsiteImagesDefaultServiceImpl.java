package com.inxedu.os.edu.service.impl.websiteimagesdefault;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.websiteimagesdefault.WebsiteImagesDefaultDao;
import com.inxedu.os.edu.entity.websiteimagesdefault.WebsiteImagesDefault;
import com.inxedu.os.edu.service.websiteimagesdefault.WebsiteImagesDefaultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板默认数据（还原用，后期根据方案优化） WebsiteImagesDefaultService接口实现
 */
@Service("websiteImagesDefaultService")
public class WebsiteImagesDefaultServiceImpl implements WebsiteImagesDefaultService {
	@Autowired
	private WebsiteImagesDefaultDao websiteImagesDefaultDao;
	
	/**
     * 添加模板默认数据（还原用，后期根据方案优化）
     */
    public Long addWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault){
		return websiteImagesDefaultDao.addWebsiteImagesDefault(websiteImagesDefault);
    }
    
    /**
     * 删除模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     */
    public void delWebsiteImagesDefaultByImageId(Long imageId){
    	websiteImagesDefaultDao.delWebsiteImagesDefaultByImageId(imageId);
    }
    
    /**
     * 修改模板默认数据（还原用，后期根据方案优化）
     * @param websiteImagesDefault
     */
    public void updateWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault){
    	websiteImagesDefaultDao.updateWebsiteImagesDefault(websiteImagesDefault);
    }
    
    /**
     * 通过imageId，查询模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     * @return
     */
    public WebsiteImagesDefault getWebsiteImagesDefaultByImageId(Long imageId){
    	return websiteImagesDefaultDao.getWebsiteImagesDefaultByImageId(imageId);
    }
    
    /**
     * 分页查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @param page 分页条件
     * @return List<WebsiteImagesDefault>
     */
    public List<WebsiteImagesDefault> queryWebsiteImagesDefaultListPage(WebsiteImagesDefault websiteImagesDefault,PageEntity page){
    	return websiteImagesDefaultDao.queryWebsiteImagesDefaultListPage(websiteImagesDefault, page);
    }
    
    /**
     * 条件查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @return List<WebsiteImagesDefault>
     */
    public List<WebsiteImagesDefault> queryWebsiteImagesDefaultList(WebsiteImagesDefault websiteImagesDefault){
    	return websiteImagesDefaultDao.queryWebsiteImagesDefaultList(websiteImagesDefault);
    }
}



