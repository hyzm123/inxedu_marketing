package com.inxedu.os.edu.entity.Template;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 申请讲师实体
 * @author www.inxedu.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TemplateFile implements Serializable {

    private int id;//
    private String name;// 文件名
    private String filePath;// 文件路径
    private String fileType;// 文件夹还是文件
    private String parentFileName = "";// 文件夹还是文件

}
