package com.inxedu.os.common.cache;

import java.util.Map;
import java.util.Set;

/**
 * @author www.inxedu.com
 */
public interface MemCacheService {
    Object get(String var1);

    boolean set(String var1, Object var2);

    Map<String, Object> getBulk(Set<String> var1);

    boolean remove(String var1);

    boolean set(String var1, Object var2, int var3);
}
