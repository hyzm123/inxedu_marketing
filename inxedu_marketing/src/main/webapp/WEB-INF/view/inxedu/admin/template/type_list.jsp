<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/30
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp"%>
<html>
<head>
   <%-- <link type="text/css" rel="stylesheet" href="${ctx}/static/common/ztree/css/zTreeStyle.css" />
    <script type="text/javascript" src="${ctx}/static/common/uploadify/ccswfobject.js"></script>
    <link rel="stylesheet" type="text/css" href="${ctx}/kindeditor/themes/default/default.css" />
    <script type="text/javascript" src="${ctx}/kindeditor/kindeditor-all.js"></script>
    <script type="text/javascript" src="${ctx}/static/common/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
--%>
    <script type="text/javascript" src="${ctx}/static/common/uploadify/ccswfobject.js"></script>
    <link rel="stylesheet" type="text/css" href="${ctx}/kindeditor/themes/default/default.css" />
    <script type="text/javascript" src="${ctx}/kindeditor/kindeditor-all.js"></script>
    <script type="text/javascript" src="${ctx}/static/common/uploadify/swfobject.js"></script>
    <script type="text/javascript" src="${ctx}/static/common/uploadify/jquery.uploadify.v2.1.4.min.js"></script>

    <title>模板管理</title>
    <style>
        img{
            width: 64px;
            height: 64px;
        }
        li{
            display: inline;
        }
        .file-name{
            text-align: center;
        }
        ol{
            margin-top: 30px;
        }
        .file{
            width: 80px;
            height: 100px;
            float: left;
            margin: 20px;
        }
        .file:hover p{
            color: #ff5e50;
        }
        .file p{
            color: #ff5e50;
        }
        .file a:hover{
            text-decoration: none;
        }
    </style>
    <script>
        /*$(function() {
            $('#file_upload').uploadify({
            'swf'      : '/uploadify uploadify.swf',   //指定上传控件的主体文件
            'uploader' : '/uploadify uploadify.php'    //指定服务器端上传处理文件
            //其他配置项
        });
        });*/
        $(function(){
            //showKpointZtree(ztree);
            //音频上传控件
            //uploadAudio('audioupload','videourl','audioQueue');
            //文档上传控件

            uploadFileLoad('file_upload','videourl','fileQueue');
            //文本编辑框
            //initKindEditor_addblog('content', 580, 350,'courseContxt','true');
        });
        /**
         * 文档上传控件加载
         * @param controlId
         * @param ids
         * @param errId
         */
        function uploadFileLoad(controlId,ids,errId){
            $("#"+controlId).uploadify({
                'uploader' : '${ctximg}/static/common/uploadify/uploadify.swf', //上传控件的主体文件，flash控件  默认值='uploadify.swf'
                'script' :'<%=uploadfileToTemplate%>',
                'scriptData':{"param":"courseKpoint/pdf","width":"128","height":"78"},
                'queueID' : 'fileQueue', //文件队列ID
                'fileDataName' : 'fileupload', //您的文件在上传服务器脚本阵列的名称
                'auto' : true, //选定文件后是否自动上传
                'multi' :false, //是否允许同时上传多文件
                'hideButton' : false,//上传按钮的隐藏
                'buttonText' : 'Upload',//默认按钮的名字
                'buttonImg' : '/static/common/uploadify/upload.png',//使用图片按钮，设定图片的路径即可
                'width' : 105,
                'simUploadLimit' : 3,//多文件上传时，同时上传文件数目限制
                'sizeLimit' : 51200000,//控制上传文件的大小
                'queueSizeLimit' : 3,//限制在一次队列中的次数（可选定几个文件）
                'fileDesc' : '支持格式:.*',
                'fileExt'  : '*;',
                'cancelImg' : '/static/common/uploadify/cancel.png',
                onSelect : function(event, queueID,fileObj) {
                    fileuploadIndex = 1;
                    $("#"+errId).html("");
                    if (fileObj.size > 51200000) {
                        msgshow('文件太大最大限制51200kb');
                        return false;
                    }
                },
                onComplete : function(event,queueID, fileObj, response,data) {
                    //updateFunctionImageDelete("videourl","swf");
                    //返回地址不能为空
                    if(response==null||response==""){
                        $("#messagePDF").html("上传失败请刷新重试");
                        return;
                    }
                    var jsonobj = JSON.parse(response);
                    //pdf地址
                    response = jsonobj.pdfUrl;

                    //逗号隔开的地址转出数组
                    var urlList = jsonobj.pngUrlStrs.split(",");
                    //逗号隔开的缩列图地址转出数组
                    var urlListTB = jsonobj.pngUrlStrsTB.split(",");

                    var pdfPngUrlStr = "";//原图地址
                    var pdfPngUrlTBStr = "";//缩列图地址
                    for(var i=0;i<urlList.length;i++){
                        if(i==0){
                            pdfPngUrlStr=urlList[i];
                            pdfPngUrlTBStr = urlListTB[i];
                        }else{
                            pdfPngUrlStr+=","+urlList[i];
                            pdfPngUrlTBStr +="," + urlListTB[i];
                        }
                    }

                    $("#atlas").val(pdfPngUrlStr);
                    $("#atlasThumbnail").val(pdfPngUrlTBStr);
                    $("#"+ids).val(response);
                    $("#pageCount").val(urlList.length);
                },
                onError : function(event, queueID, fileObj,errorObj) {
                    $("#"+errId).html("<br/><font color='red'>"+ fileObj.name + "上传失败</font>");
                }
            });
        }
    </script>
</head>
<body>
    <div>
        <p>
            <div >
                <input  type="button" value="上传" id="file_upload"/>
            </div>
            <div style="position: absolute;top: -5px;left: 100px">
                <input id="paste" style="display: none;" type="button" value="粘 贴" class="button" onclick="changeFilePath()" />
                <input id="delete" style="display: none;" type="button" value="删除文件夹" class="button" onclick="deleteDirectory()" />
                <input id="recovery"  type="button" value="一键还原模板" class="button" onclick="recovery()"/>
                <input id="return" style="display: none;" type="button" value="返 回" class="button" onclick="returnUrl()" />
            </div>



        </p>

        <ol>
            <%--展示文件夹--%>
            <c:forEach items="${directoryName}" var="directoryName" varStatus="num">
                <div class="file">
                    <li >
                        <a href="${ctx}/admin/template/file/toChildList?path=${directoryName.value}\">
                            <img src="../../../../../images/wenjianjia.png" ><p class="file-name">${directoryName.key}</p>
                        </a>
                    </li>
                </div>

            </c:forEach>
            <%--展示文件--%>
            <c:forEach items="${fileName}" var="fileName" varStatus="num">
                <div class="file">
                    <li>
                        <a href="javascript: void(0)" onclick="getFileType('${fileName.value}')">
                            <img src="../../../../../images/wenjian.jpg" ><p class="file-name">${fileName.key}</p>
                        </a>
                    </li>
                </div>
            </c:forEach>
        </ol>
    </div>
    <script>
        var path = '${path} ';
        var hide = '${hide}';
        var moveFileName = "${moveFileName}";
        var moveFilePath="${moveFilePath}"
        if (moveFileName!=""){
            $("#paste").show();
        }else {
            $("#paste").hide();
        }
        function changeFilePath() {
            $.ajax({
                url:baselocation+'/admin/template/ajax/changeFilePath',
                type:'post',
                dataType:'json',
                data:{"moveFileName":moveFileName,"path":path,"moveFilePath":moveFilePath},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/toChildList?path="+result.message;
                    }
                }
            });
        }
        if (window.location.href==baselocation+"/admin/template/file/list"){
            $("#delete").hide()
            $("#return").hide()

        }else {
            $("#return").show()
            $("#delete").show()

        }
        function returnUrl() {
            var getUrl = window.location.href;
            var url;
            if (getUrl.substring(getUrl.length-1,getUrl.length)=='\\'){
                url = getUrl.substring(0,getUrl.lastIndexOf("\\"));
                url = url.substring(0,url.lastIndexOf("\\"));
            }else {
                url = getUrl.substring(0,getUrl.lastIndexOf("\\"))
            }

            if (url=='${ctx}/admin/template/file/toChildList?path=${ifPath}'){
                window.location.href=baselocation+"/admin/template/file/list"
            }else {
                window.location.href = url;
            }
        }
        function getFileType(name) {
            var fileType = name.substring(name.lastIndexOf(".")+1);
            if (fileType=="jsp" ||fileType=="html"|| fileType=="css"||fileType=="js"){
                window.location.href = baselocation+"/admin/template/toUpdateFile?path="+name;
            }
        }
        if (hide=="hide"){
            $("#return").hide();
            $("#delete").hide();
        }
       /* 删除文件夹*/
        function deleteDirectory() {
            var directory ="${path}";
            if(!confirm('确实要删除吗?')){
                return;
            }
            directory = directory.substring(0,directory.length-1);
            $.ajax({
                url:baselocation+'/admin/template/ajax/deleteDirectory',
                type:'post',
                dataType:'json',
                data:{"path":directory},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/list";
                    }else {
                        msgshow(result.message);
                    }
                }
            });
        }
        /*还原所有模板*/
        function recovery() {
            if (!confirm("是否还原初始模板（项目原模板将还原到初始状态）？")){
                return;
            }
            $.ajax({
                url:baselocation+'/admin/template/ajax/recovery',
                type:'post',
                dataType:'json',
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/list";
                    }else {
                        msgshow("还原失败！");
                    }
                }
            })
        }
    </script>
</body>
</html>
