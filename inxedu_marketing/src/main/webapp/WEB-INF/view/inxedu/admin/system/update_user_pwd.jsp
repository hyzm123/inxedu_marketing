<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>修改个人密码</title>
<link rel="stylesheet" href="${ctx}/static/common/nice-validator/jquery.validator.css"/>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/jquery.validator.js"></script>
<script type="text/javascript" src="${ctx}/static/common/nice-validator/local/zh-CN.js"></script>
<script type="text/javascript">

	function submitform(){
		var email = $("#email").val();
		var reg=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_])+(.[a-zA-Z0-9_])+/; //验证邮箱正则

		if(email!=''&&email!=null&&reg.test(email)==false){
			msgshow("请输入正确的邮箱");
			return
		}
		$.ajax({
			url:baselocation+'/admin/sysuser/user/updatepwd',
			type:'post',
			data:$("#submitform").serialize(),
			async:false,
			dataType:'json',
			success:function(result){
				if(!result.success){
					var entity = result.entity;
					if(entity=="oldPwdisnull"){
						msgshow("请输入旧密码");
						return;
					}
					if(entity=="newPwdisnull"){
						msgshow("请输入新密码");
						return;
					}
					if(entity=="confirmPwdisnull"){
						msgshow("请输入确认密码");
						return;
					}
					if(entity=="newPwderror"){
						msgshow("请输入6到16位的新密码");
						return;
					}
					if(entity=="newPwdNotEqualsconfirmPwd"){
						msgshow("确认密码和密码不相同");
						return;
					}
					if(entity=="oldPwdIsError"){
						msgshow("原密码不正确");
						return;
					}
					if(entity=="newPwdEquestOldPwd"){
						msgshow("旧密码和新密码相同");
						return;
					}

				}
				msgshow("成功");
			}
		});
	}
</script>
</head>
<body>

<div class="rMain">
	<div class="">
		<form action="${ctx}/admin" method="post" id="submitform">
			<p class="hLh30 fsize20 c-333 f-fH mt30">密码修改
				<tt class=" ml20 fsize16">
					（<font color="red">*</font>&nbsp;为必填项）
				</tt>
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red">*</font>&nbsp;昵称:</label>
				<input type="text" name="userName" id="userName" value="${sysUser.userName}"data-rule="required;" class="{required:true} lf" />
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red">*</font>&nbsp;邮箱:</label>
				<input type="text" name="email" id="email" value="${sysUser.email}" data-rule="required;"class="{required:true} lf" />
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red"></font>&nbsp;原密码:</label>
				<input type="password" name="oldPwd" id="oldPwd"  class="{required:true} lf"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red"></font>&nbsp;新密码：</label>
				<input type="password" name="newPwd" id="newPwd" class="{required:true,number:true,min:0,max:1000} lf"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<label for="sf"><font color="red"></font>&nbsp;确认密码：</label>
				<input type="password" name="confirmPwd" id="confirmPwd" class="{required:true} lf"/>
				<span class="field_desc"></span>
			</p>
			<p>
				<input type="button" value="确 定" class="button" onclick="submitform()" />
				<%--<input type="button" value="返 回" class="button" onclick="javascript:history.go(-1);" />--%>
			</p>
		</form>
	</div>
	<!-- /tab4 end -->
</div>
</body>
</html>
