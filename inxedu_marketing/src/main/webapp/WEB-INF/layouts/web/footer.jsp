<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<footer id="footer">
	<section class="container">
		<div class="of">
			<h4 class="hLh30"><span class="fsize18 f-fM c-999">友情链接</span></h4>
			<ul class="clearfix flink-list">
				<c:forEach items="${navigatemap.FRIENDLINK}" var="friendLinkNavigate">
					<li><a href="${friendLinkNavigate.url}" title="${friendLinkNavigate.name}" <c:if test="${friendLinkNavigate.newPage==0}">target="_blank"</c:if>>${friendLinkNavigate.name}</a></li>
				</c:forEach>
			</ul>
		</div>
		<div class="b-foot">
			<section class="fl col-7">
				<section class="mr20">
					<section class="b-f-link">
						<c:forEach items="${navigatemap.TAB}" var="indexNavigate" varStatus="index">
							<a href="${indexNavigate.url}" title="${indexNavigate.name}" <c:if test="${indexNavigate.newPage==0}">target="_blank"</c:if>>${indexNavigate.name}</a>|
						</c:forEach>
						<a href="/front/appleteacher">教师申请</a>|
						<span>服务热线：${websitemap.web.phone}</span>
						<span>Email：${websitemap.web.email}</span>


					</section>
					<section class="b-f-link mt10">
						<span>${websitemap.web.copyright}</span>	
						<!-- 统计代码 -->
						${tongjiemap.censusCode.censusCodeString}
					</section>
					<!-- <section class="b-f-link mt10">
						<span>Powered by</span>
						<a href="" title="" style="margin-left: 0;">因酷软件</a>
					</section> -->
				</section>
			</section>
			<aside class="fl col-3 tac mt15">
				<section class="gf-tx">
					<span><img src="${ctx}/static/mooc_web/img/wx-icon.png" alt=""></span>
					<div class="gf-tx-ewm">
						<c:forEach var="image" items="${websiteImages.type_11}" varStatus="status">
							<c:if test="${status.count==1 }">
							<img src="<%=staticImage%>${image.imagesUrl}" alt="">
							</c:if>
						</c:forEach>
					</div>
				</section>
				<section class="gf-tx">
					<span><img src="${ctx}/static/mooc_web/img/wb-icon.png" alt=""></span>
					<div class="gf-tx-ewm">
						<c:forEach var="image" items="${websiteImages.type_11}" varStatus="status">
							<c:if test="${status.count==2 }">
							<img src="<%=staticImage%>${image.imagesUrl}" alt="">
							</c:if>
						</c:forEach>
					</div>
				</section>
			</aside>
			<div class="clear"></div>
		</div>
	</section>
</footer>
<!-- /公共 客服，官信，返回顶部  开始-->
<div class="r-fixed-wrap r-fix-box">
	<ul class="r-fixed-ul">
		<li id="goTopBtn" class="undis">
			<a href="javascript: void(0)" title="返回顶部" class="bg-master"><em class="r-f-icon-3">&nbsp;</em><span>返回顶部</span></a>
		</li>
<%--		<li class="foot-zixun" id="shopCart">
				<a href="javascript:shopCartHtml()" title="查看详情" class="bg-master pr" id="v-nav-first">
					<em class="r-f-icon-4">&nbsp;</em><!-- <span>查看详情</span> -->
					<tt class="shop-car-num" id="shopCartNum">0</tt>
				</a>
			</li>--%>
	<%--	<li class="foot-zixun">
			<a href="" title="在线咨询" class="bg-master"><em class="r-f-icon-1">&nbsp;</em><span>在线咨询</span></a>
		</li>--%>
		<%--<li class="foot-zixun">
			<a href="" title="扫描关注" class="bg-master"><em class="r-f-icon-2">&nbsp;</em><span>扫描关注</span></a>
		</li>--%>
		<li class="foot-zixun">
			<a href="JavaScript:void(0)" title="扫描关注" class="bg-master">
				<em class="r-f-icon-2">&nbsp;</em><span>扫描关注</span>
				<div class="smgz-pic">
					<img src="<%=contextPath%>${onlinemap.online.onlineImageUrl }" width="150" height="150" class="dis"/>
					<p class=" fsize14 f-fM c-666 tac">扫一扫，关注我们</p>
				</div>
			</a>
		</li>
	</ul>
	<div class="shopcar-box fr" id="shopcarthtml">
		<div class=" s-car-box">
			<div class="s-car-box-top clearfix">
				<div class="fl s-c-icon">
					<em class="icon20 vam"></em>
					<span class="vam fsize16 ml5 c-666">购物车</span>
				</div>
				<div class="fr mt5">
					<a href="javascript:ccrFun();" class="s-car-close"></a>
				</div>
			</div>
			<div class="s-car-box-middle">
				<!--无内容开始  -->
				<section class="no-data-wrap">
					<em class="icon30 no-data-ico">&nbsp;</em>
					<span class="c-666 fsize14 ml10 vam">
						<font class="c-999 fsize12 vam">购物车暂无课程，建议你去<a class="c-master ml5" title="选课" href="" target="_blank">选课</a></font>
					</span>
				</section>
				<!--无内容结束  -->

			</div>
			<div class="s-car-box-bot">
				<div class="s-c-b c-4e">
					<div class="hLh20"><strong class="c-master"></strong>件商品</div>
					<div class="hLh20">共计：<strong class="c-master">￥0</strong></div>
					<a target="_blank" href="http://demo1.inxedu.com/shopcart?command=queryShopCart" class="js-btn">立即结算</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /公共 客服，官信，返回顶部  结束-->
<script type="text/javascript" src="${ctx}/static/mooc_web/front/footer.js"></script>